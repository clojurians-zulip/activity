(ns streams
  "Compile stream data from local and remote data.

  local data should be enough to seed the streams-table on the site.
  Though when providing remote data it will be more up to date,
  i.e. containing new streams and current subscriber-counts.

  With both provided, local is leading for:
  - name
  - description
  - anything that remote does not contain

  Remote is leading for:
  - subscriber-count
  - weekly-traffic
  "
  (:import
   [org.htmlcleaner HtmlCleaner DomSerializer CleanerProperties])
  (:require
   [clojure.data.json :as json]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.pprint :as pprint]
   [clojure.string :as str]
   [clj-xpath.core :refer [$x:text*]]))


(defn- html->xml
  [a-html-doc]
  (let [cleaner       (new HtmlCleaner)
        cleaner-props (new CleanerProperties)
        dom-srlzr     (new DomSerializer cleaner-props)
        cleaned-doc   (.clean cleaner a-html-doc)]
    (.createDOM dom-srlzr cleaned-doc)))


(defn- extract-org-data [xml]
  (let [clojurify #(keyword (str/replace % "_" "-"))
        attr-data (->> xml
                       ($x:text* "//div[@id='page-params']/@data-params")
                       first)]
    (-> attr-data
        (str/replace #"&quot;" "\"")
        (json/read-str :key-fn clojurify))))
(comment
  (->> data ($x:text* "//div[@id='page-params']/@data-params") first)
  (raw-public-streams (extract-org-data data))
  (get-in (extract-org-data data) [:state-data :never-subscribed]))

(defn- raw-public-streams [{{:keys [unsubscribed subscriptions never-subscribed]} :state-data :as _data}]
  (remove (some-fn :invite-only :is-announcement-only)
          (-> subscriptions (into never-subscribed) (into unsubscribed))))

(defn- remote-stream->local [{:keys [stream-id name description subscribers stream-weekly-traffic] :as _stream}]
  {:id               stream-id
   :name             name
   :description      description
   :subscriber-count (count subscribers)
   :websites         []
   :weekly-traffic   (or stream-weekly-traffic 0)})


(defn- extract-streams
  "Yields streams from remote-data in preferred local format, sorted by name"
  [data]
  (->> data
      raw-public-streams
      (map remote-stream->local)
      (sort-by (comp str/lower-case :name))))


(defn- file-exists? [filename]
  (-> filename (io/file) .exists))

(comment
  (def data (-> "resources/zulip.html" (slurp) html->xml #_extract-org-data)))

(defmulti printer (fn [_streams target]
                    (-> target
                        (str/split #"\.")
                        second
                        (keyword))))


(defmethod printer :json [streams target]
  (let [writer  (if (str/starts-with? target "-")
                  *out* (io/writer target))
        jsonify #(-> % name (str/replace "-" "_"))]
    (binding [*out* writer]
      (json/pprint streams :key-fn jsonify))))


(defmethod printer :edn [streams target]
  (let [writer (if (str/starts-with? target "-")
                 *out* (io/writer target))]
    (binding [clojure.pprint/*print-right-margin* 40]
      (clojure.pprint/pprint streams writer))))


(defn ^#:inclined{:option.remote-html {:desc "Remote source (default: resources/zulip.html)"}
                  :option.local-edn   {:desc "Local source (default: resources/streams.edn)"}
                  :option.target      {:desc    "Where and in what format to write. E.g. '-.json' to write json to stdout, 'streams.json' to write it to a file."
                                       :default "-.edn"}}
  pprint
  "Combines local and remote (if any) data and writes result"
  [{:keys [local-edn remote-html target]}]
  {:pre [(or (nil? local-edn) (file-exists? local-edn))
         (or (nil? remote-html) (file-exists? remote-html))]}
  (let [local-edn          (or local-edn "resources/streams.edn")
        remote-html        (or remote-html "resources/zulip.html")
        by-id              #(->> %
                                 (map (juxt :id identity))
                                 (into {}))
        local-streams      (if-not (file-exists? local-edn)
                             {}
                             (-> local-edn slurp edn/read-string by-id))
        remote-streams     (if-not (file-exists? remote-html)
                             {}
                             (-> remote-html
                                 slurp
                                 html->xml
                                 extract-org-data
                                 extract-streams
                                 by-id))
        select-stream-keys #(into {}
                                  (map (fn [[id stream]]
                                         [id (select-keys stream %2)])
                                       %1))
        merged-streams     (merge-with merge
                                       remote-streams
                                       local-streams
                                       (select-stream-keys remote-streams
                                                           [:subscriber-count :weekly-traffic]))
        to-write           (->> merged-streams
                                vals
                                (sort-by (comp str/lower-case :name))
                                (into []))]
    (printer to-write target)))
