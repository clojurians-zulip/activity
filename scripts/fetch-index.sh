#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

rm -rf ./session*
token=$(curl --cookie-jar ./session --cookie ./session 'https://clojurians.zulipchat.com/login/' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:74.0) Gecko/20100101 Firefox/74.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' -H 'TE: Trailers' | grep csrfmiddlewaretoken | head -n 1 | cut -d$'"' -f 6)

curl -L --cookie-jar ./session --cookie ./session 'https://clojurians.zulipchat.com/accounts/login/?next=' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:74.0) Gecko/20100101 Firefox/74.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: https://clojurians.zulipchat.com' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: https://clojurians.zulipchat.com/login/' -H 'Upgrade-Insecure-Requests: 1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data "csrfmiddlewaretoken=${token}&username=${ZULIP_EMAIL}&password=${ZULIP_PASSWORD}&button="
